package lib

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

// Define interface for providing input
type AocPuzzle interface {
    GetInput() ([]string, error)
}


// Define web input
type WebInput struct {
    Day       int
    Year      int
    Token     string
    CachePath string
}

type WebInputError struct {
    ErrorString string
}

func (e *WebInputError) Error() string {
    return e.ErrorString
}

func (w *WebInput) cachedFile() string {
    return fmt.Sprintf("%v/%v.txt", w.CachePath, w.Day)
}

func (w *WebInput) inCache() bool {
    if _, err := os.Stat(w.cachedFile()); err == nil {
        return true
    }
    return false
}

func (w *WebInput) makeCacheIfMissing() error {
    if _, err := os.Stat(w.CachePath); err != nil {
        log.Printf("Cache doesnt exist, creating it at: %v\n", w.CachePath)
        return os.MkdirAll(w.CachePath, 0750)
    }
    return nil
}

func (w *WebInput) readFromCache() ([]string, error) {
    f, err := os.Open(w.cachedFile())
    if err != nil {
        return nil, err
    }
    input, err := ioutil.ReadAll(f)
    if err != nil {
        return nil, err
    }

    return strings.Split(string(input[:]), "\n"), nil
}

func (w *WebInput) writeToCache(content io.ReadCloser) error {
    f, err := os.Create(w.cachedFile())
    if err != nil {
        return err
    }
    defer f.Close()

    inputBody, err := ioutil.ReadAll(content)
    if err != nil {
        return err
    }

    f.Write(inputBody)
    return nil
}

func (w *WebInput) fetchFromWeb() error {
    log.Printf("Fetching Day %v from web\n", w.Day)
    client := &http.Client{}

    // Build our request
    requestUrl := fmt.Sprintf("https://adventofcode.com/%v/day/%v/input", w.Year, w.Day)
    sessionToken := fmt.Sprintf("session=%v", w.Token)
    req, err := http.NewRequest("GET", requestUrl, nil)
    if err != nil {
        return err
    }
    req.Header.Add("Cookie", sessionToken)

    // Run the request
    res, err := client.Do(req)
    if err != nil {
        return err
    }
    if res.StatusCode != 200 {
        return &WebInputError{ErrorString: fmt.Sprintf("Failed to fetch %v got error code %v", requestUrl, res.StatusCode)}
    }

    // Cache the results
    w.writeToCache(res.Body)

    return nil
}

func (w *WebInput) GetInput() ([]string, error) {
    w.makeCacheIfMissing()
    if !w.inCache() {
        w.fetchFromWeb()
    }

    return w.readFromCache()
}

func WebInputFactory(day int, year int) (*WebInput, bool) {
    // Fetch out session token
    sessionToken, ok := os.LookupEnv("AOC_SESSION_TOKEN")
    if !ok {
        return nil, false
    }

    // Check where we are caching, default to .cache
    cachePath, ok := os.LookupEnv("AOC_CACHE_PATH")
    if !ok {
        cachePath = ".cache"
    }

    return &WebInput{
        Day: day,
        Year: year,
        Token: sessionToken,
        CachePath: cachePath,
    }, true
}

// Simple AoCInput Used for Testing
type SimpleInput struct {
    Input []string
}

func (s *SimpleInput) GetInput() ([]string, error) {
    return s.Input, nil
}
