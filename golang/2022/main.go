package main

import (
    "fmt"
    "sync"

    "gitlab.com/scgodbold/advent-of-code/golang/2022/day/one"
    "gitlab.com/scgodbold/advent-of-code/golang/2022/day/two"
    "gitlab.com/scgodbold/advent-of-code/golang/2022/day/three"
    "gitlab.com/scgodbold/advent-of-code/golang/2022/day/four"
)

func main() {
    fmt.Println("Starting Advent of code")

    var wg sync.WaitGroup

    // Day One
    wg.Add(1)
    go one.Solve(&wg)
    // Day Two
    wg.Add(1)
    go two.Solve(&wg)
    // Day Three
    wg.Add(1)
    go three.Solve(&wg)
    // Day Four
    wg.Add(1)
    go four.Solve(&wg)

    // Wait for completion
    wg.Wait()
}
