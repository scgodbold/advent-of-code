package {day}_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/{day}"
)

func getSample() lib.AocPuzzle {
    return &lib.SimpleInput{
        Input: []string{
        },
    }
}

func TestPartOne(t *testing.T) {
    expected := "answer"
    result, err := {day}.PartOne(getSample())
    if err != nil {
        t.Logf("Expected no errors for part one got: %v\n", err.Error())
        t.Fail()
    }
    if result != expected {
        t.Logf("Expected to get answer of %v, got %v\n", expected, result)
        t.Fail()
    }
}


func TestPartTwo(t *testing.T) {
    expected := "answer"
    result, err := {day}.PartTwo(getSample())
    if err != nil {
        t.Logf("Expected no errors for part one got: %v\n", err.Error())
        t.Fail()
    }
    if result != expected {
        t.Logf("Expected to get answer of %v, got %v\n", expected, result)
        t.Fail()
    }
}
