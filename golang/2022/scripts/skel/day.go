package {day}

import (
	"log"
	"sync"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

const puzzleDay={dayNum}
const puzzleYear=2022

func PartOne(input lib.AocPuzzle) (string, error) {
    return "", &day.AocError{ ErrorString: "Unimplmented" }
}

func PartTwo(input lib.AocPuzzle) (string, error) {
    return "", &day.AocError{ ErrorString: "Unimplmented" }
}

func Solve(wg *sync.WaitGroup) {
    defer wg.Done()
    d, ok := day.Factory(puzzleDay, puzzleYear, PartOne, PartTwo)
    if !ok {
        log.Printf("Failed to initialize day 3\n")
        return
    }
    d.Solve()
}
