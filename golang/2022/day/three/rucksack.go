package three

import (
	"fmt"
	"strings"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

type Item rune

// Handle our Item Valuation
func (i Item) Value() int {
    number := int(i)

    if number >= 65 && number <=90 {
        return number - 38
    }
    return number - 96
}

func (i Item) ToString() string {
    return string(i)
}

type Compartment map[Item]bool

func (c Compartment) ToString() string {
    parts := []string{}
    for k := range c {
        parts = append(parts, string(k))
    }
    return strings.Join(parts, ",")
}

// Construct the Rucksack of Items
type Rucksack struct {
    Primary Compartment
    Secondary Compartment
}

func (r *Rucksack) Dupe() Item {
    return r.Dupes()[0]
}

func (r *Rucksack) Dupes() []Item {
    dupes := []Item{}
    for k := range r.Primary {
        if _, ok := r.Secondary[k]; ok {
            dupes = append(dupes, k)
        }
    }
    return dupes
}

func (r* Rucksack) Contains(i Item) bool {
    if _, ok := r.Primary[i]; ok {
        return true
    }
    if _, ok := r.Secondary[i]; ok {
        return true
    }
    return false
}

func (r* Rucksack) All() []Item {
    parts := []Item{}
    for k := range r.Primary {
        parts = append(parts, k)
    }
    for k := range r.Secondary {
        parts = append(parts, k)
    }
    return parts
}

func (r *Rucksack) ToString() string {
    return fmt.Sprintf("[(%v), (%v)]\n", r.Primary.ToString(), r.Secondary.ToString())
}

func (r* Rucksack) Populate(input string) {
    mid := len(input)/2
    for _, v := range input[:mid] {
        r.Primary[Item(v)] = true
    }
    for _, v := range input[mid:] {
        r.Secondary[Item(v)] = true
    }
}

func RuckSackFactory(input string) *Rucksack {
    r := &Rucksack{Primary: map[Item]bool{}, Secondary: map[Item]bool{}}
    r.Populate(input)
    return r
}

func RuckSacksFactory(input lib.AocPuzzle) ([]*Rucksack, error) {
    sacks := []*Rucksack{}
    puzzleInput, err := input.GetInput()

    if err != nil {
        return sacks, err
    }

    for _, v := range puzzleInput {
        if v == "" {
            continue
        }
        sacks = append(sacks, RuckSackFactory(v))
    }

    return sacks, nil
}
