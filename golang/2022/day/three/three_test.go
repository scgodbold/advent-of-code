package three_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/three"
)

func getSample() lib.AocPuzzle {
    return &lib.SimpleInput{
        Input: []string{
            "vJrwpWtwJgWrhcsFMMfFFhFp",
            "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
            "PmmdzqPrVvPwwTWBwg",
            "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
            "ttgJtRGJQctTZtZT",
            "CrZsJsPPZsGzwwsLwLmpwMDw",
        },
    }
}

func TestSampleInputPartOne(t *testing.T) {
    input := getSample()
    expected := "157"

    result, err := three.PartOne(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}

func TestSampleInputPartTwo(t *testing.T) {
    input := getSample()
    expected := "70"

    result, err := three.PartTwo(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}
