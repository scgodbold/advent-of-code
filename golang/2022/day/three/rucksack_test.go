package three_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/three"
)

func TestItemValueTranslation(t *testing.T) {
    type testCase struct {
        Input three.Item
        Expected int
    }

    tests := []testCase{
        {Input: 'A', Expected: 27},
        {Input: 'Z', Expected: 52},
        {Input: 'a', Expected: 1},
        {Input: 'z', Expected: 26},
    }


    for _, test := range tests {
        result := test.Input.Value()
        if result != test.Expected {
            t.Logf("Error %v should be have value %v but got %v\n", test.Input.ToString(), test.Expected, result)
            t.Fail()
        }
    }
}

func TestRuckSackDupes(t *testing.T) {
    type testCase struct {
        Input *three.Rucksack
        Expected int
    }
    tests := []testCase{
        // No Dupes
        {
            Input: &three.Rucksack{ Primary: map[three.Item]bool { 'a': true, 'b': true, 'c': true}, Secondary: map[three.Item]bool {'d': true, 'e': true, 'f': true},},
            Expected: 0,
        },
        // No One Dupe
        {
            Input: &three.Rucksack{ Primary: map[three.Item]bool { 'a': true, 'b': true, 'c': true}, Secondary: map[three.Item]bool {'d': true, 'e': true, 'a': true},},
            Expected: 1,
        },
        // Multi Dupe
        {
            Input: &three.Rucksack{ Primary: map[three.Item]bool { 'a': true, 'b': true, 'c': true}, Secondary: map[three.Item]bool {'a': true, 'c': true, 'e': true},},
            Expected: 2,
        },
    }

    for _, test := range tests {
        result := test.Input.Dupes()
        // Check we get the correct amounts
        if len(result) != test.Expected {
            t.Logf("For Rucksack %v: expected %v dupes, got %v\n", test.Input.ToString(), test.Expected, len(result))
            t.Fail()
        }
        // Not checking specific ordering cause it seems inconsistent on its return,
        // probably my lack of understanding append or maps in go
    }
}

func TestRuckSackFactory(t *testing.T) {
    r := three.RuckSackFactory("abcdef")
    for _, v := range "abc" {
        if _, ok := r.Primary[three.Item(v)]; !ok {
            t.Logf("Expected %v to be in the primary compartment, but was missing", string(v))
            t.Fail()
        }
    }
    for _, v := range "def" {
        if _, ok := r.Secondary[three.Item(v)]; !ok {
            t.Logf("Expected %v to be in the secondary compartment, but was missing", string(v))
            t.Fail()
        }
    }
}

func TestRucksackContains(t *testing.T) {
    r := three.RuckSackFactory("abcdef")
    // Miss
    if r.Contains(three.Item('z')) {
        t.Logf("Rucksack %v said it contains 'z' but it should not", r.ToString())
        t.Fail()
    }
    // Hit Primary
    if !r.Contains(three.Item('a')) {
        t.Logf("Rucksack %v said it does not contain 'a' but it should not", r.ToString())
        t.Fail()
    }
    // Hit Secondary
    if !r.Contains(three.Item('f')) {
        t.Logf("Rucksack %v said it does not contain 'f' but it should not", r.ToString())
        t.Fail()
    }
}
