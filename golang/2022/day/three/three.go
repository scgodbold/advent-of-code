package three

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

func intersectRucksacks(rucksacks []*Rucksack) Item {
    for _, item := range rucksacks[0].All() {
        if rucksacks[1].Contains(item) && rucksacks[2].Contains(item) {
            return item
        }
    }
    return '1'
}

func PartOne(input lib.AocPuzzle) (string, error) {
    rucksacks, err := RuckSacksFactory(input)
    if err != nil {
        return "-1", err
    }
    score := 0
    for _, r := range rucksacks {
        score += r.Dupe().Value()
    }
    return fmt.Sprintf("%v", score), nil
}

func PartTwo(input lib.AocPuzzle) (string, error) {
    rucksacks, err := RuckSacksFactory(input)
    if err != nil {
        return "-1", err
    }
    i := 0
    score := 0
    for i <= len(rucksacks)-3{
        score += intersectRucksacks(rucksacks[i:i+3]).Value()
        i += 3
    }
    return fmt.Sprintf("%v",score), nil
}

func Solve(wg *sync.WaitGroup) {
    defer wg.Done()
    d, ok := day.Factory(3, 2022, PartOne, PartTwo)
    if !ok {
        log.Printf("Failed to initialize day 3\n")
        return
    }
    d.Solve()
}
