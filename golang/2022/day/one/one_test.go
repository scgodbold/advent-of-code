package one_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/one"
)

func TestSampleInputPartOne(t *testing.T) {
    i := &lib.SimpleInput{
        Input: []string {
            "1000",
            "2000",
            "3000",
            "",
            "4000",
            "",
            "5000",
            "6000",
            "",
            "7000",
            "8000",
            "9000",
            "",
            "10000",
        },
    }
    expected := "24000"

    result, err := one.PartOne(i)
    if err != nil {
        t.Logf("Error should be nil but got %v\n", err.Error())
        t.Fail()
    }
    if result != expected {
        t.Logf("Should be %v but got %v\n", expected, result)
        t.Fail()
    }
}

func TestSampleInputPartTwo(t *testing.T) {
    i := &lib.SimpleInput{
        Input: []string {
            "1000",
            "2000",
            "3000",
            "",
            "4000",
            "",
            "5000",
            "6000",
            "",
            "7000",
            "8000",
            "9000",
            "",
            "10000",
        },
    }
    expected := "45000"

    result, err := one.PartTwo(i)
    if err != nil {
        t.Logf("Error should be nil but got %v\n", err.Error())
        t.Fail()
    }
    if result != expected {
        t.Logf("Should be %v but got %v\n", expected, result)
        t.Fail()
    }
}
