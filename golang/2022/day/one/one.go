package one

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"sync"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

// Define Elf Contents
type Elf struct {
    Foods []int
}

func (e *Elf) AddFood(food int) {
    e.Foods = append(e.Foods, food)
}

func (e *Elf) TotalCalories() int {
    total := 0
    for _, v := range e.Foods {
        total += v
    }

    return total
}

// OverArching Functions
func Solve(wg *sync.WaitGroup) {
    defer wg.Done()
    d, ok := day.Factory(1, 2022, PartOne, PartTwo)
    if !ok {
        log.Printf("Failed to initialize day 1\n")
        return
    }
    d.Solve()
}

func PopulateElves(input lib.AocPuzzle) ([]*Elf, error){
    elves := []*Elf{}
    elf := &Elf{Foods: []int{}}
    inputValues, err := input.GetInput()
    if err != nil {
        return nil, err
    }
    for _, v := range inputValues {
        food, err := strconv.Atoi(v)
        if err != nil {
            elves = append(elves, elf)
            elf = &Elf{Foods: []int{}}
            continue
        }
        elf.AddFood(food)
    }
    elves = append(elves, elf)

    return elves, nil
}

func HighestNElves(elves []*Elf, n int) []*Elf {
    sort.Slice(elves, func(i, j int) bool {
        return elves[i].TotalCalories() > elves[j].TotalCalories()
    })
    return elves[0:n]
}

func PartOne(input lib.AocPuzzle) (string, error) {
    elves, err := PopulateElves(input)
    if err != nil {
        return "-1", err
    }

    mostCalories := HighestNElves(elves, 1)

    return fmt.Sprintf("%v", mostCalories[0].TotalCalories()), nil
}

func PartTwo(input lib.AocPuzzle) (string, error) {
    elves, err := PopulateElves(input)
    if err != nil {
        return "-1", err
    }

    mostCalories := HighestNElves(elves, 3)

    totalCalories := 0
    for _, v := range mostCalories {
        totalCalories += v.TotalCalories()
    }

    return fmt.Sprintf("%v", totalCalories), nil
}


