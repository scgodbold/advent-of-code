package four_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/four"
)

func getSample() lib.AocPuzzle {
    return &lib.SimpleInput{
        Input: []string{
            "2-4,6-8",
            "2-3,4-5",
            "5-7,7-9",
            "2-8,3-7",
            "6-6,4-6",
            "2-6,4-8",
        },
    }
}

func TestSampleInputPartOne(t *testing.T) {
    input := getSample()
    expected := "2"

    result, err := four.PartOne(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}

func TestSampleInputPartTwo(t *testing.T) {
    input := getSample()
    expected := "4"

    result, err := four.PartTwo(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}
