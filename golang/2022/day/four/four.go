package four

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

const puzzleDay=4
const puzzleYear=2022

func PartOne(input lib.AocPuzzle) (string, error) {
    orders, err := OrdersFactory(input)
    if err != nil {
        return "-1", err
    }
    count := 0
    for _, order := range orders {
        if order.Contains() {
            count += 1
        }
    }
    return fmt.Sprintf("%v", count), nil
}

func PartTwo(input lib.AocPuzzle) (string, error) {
    orders, err := OrdersFactory(input)
    if err != nil {
        return "-1", err
    }
    count := 0
    for _, order := range orders {
        if order.Overlaps() {
            count += 1
        }
    }
    return fmt.Sprintf("%v", count), nil
}

func Solve(wg *sync.WaitGroup) {
    defer wg.Done()
    d, ok := day.Factory(puzzleDay, puzzleYear, PartOne, PartTwo)
    if !ok {
        log.Printf("Failed to initialize day 3\n")
        return
    }
    d.Solve()
}
