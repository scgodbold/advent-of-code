package four

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

type Assignment struct {
    Start int
    End int
}

func (a *Assignment) Contains(other *Assignment) bool {
    return a.Start <= other.Start && a.End >= other.End
}

func (a *Assignment) Overlaps(other *Assignment) bool {
    return other.Start <= a.End && other.End >= a.Start
}

func (a *Assignment) ToString() string {
    return fmt.Sprintf("%v-%v", a.Start, a.End)
}

func AssignmentFactory(input string) (*Assignment, error) {
    assignment := &Assignment{
}
    splits := strings.Split(input, "-")
    start, err := strconv.Atoi(splits[0])
    if err != nil {
        return nil, err
    }
    end, err := strconv.Atoi(splits[1])
    if err != nil {
        return nil, err
    }
    assignment.Start=start
    assignment.End=end
    return assignment, nil
}

type Order struct {
    First *Assignment
    Second *Assignment
}

func (o *Order) Contains() bool {
    return o.First.Contains(o.Second) || o.Second.Contains(o.First)
}

func (o *Order) Overlaps() bool {
    return o.First.Overlaps(o.Second) || o.Second.Overlaps(o.First)
}

func (o *Order) ToString() string {
    return fmt.Sprintf("(%v, %v)", o.First.ToString(), o.Second.ToString())
}


func OrderFactory(input string) (*Order, error) {
    orders :=  &Order{}
    splits := strings.Split(input, ",")
    first, err := AssignmentFactory(splits[0])
    if err != nil {
        return nil, err
    }
    second, err := AssignmentFactory(splits[1])
    if err != nil {
        return nil, err
    }
    orders.First = first
    orders.Second = second
    return orders, nil
}

func OrdersFactory(input lib.AocPuzzle) ([]*Order, error) {
    puzzleInput, err := input.GetInput()
    if err != nil {
        return nil, err
    }

    orders := []*Order{}
    for _, v := range puzzleInput {
        if v == "" {
            continue
        }
        newOrder, err := OrderFactory(v)
        if err != nil {
            return nil, err
        }
        orders = append(orders, newOrder)
    }
    return orders, nil
}
