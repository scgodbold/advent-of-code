package four_test

import (
	"testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/four"
)

func TestAssignmentContains(t *testing.T) {
    type testCase struct {
        Input *four.Assignment
        Expected bool
    }
    static := &four.Assignment{Start: 3, End: 8}

    tests := []testCase{
        // Contained
        {Input: &four.Assignment{Start:4, End: 6}, Expected: true},
        // Miss lower
        {Input: &four.Assignment{Start:1, End: 6}, Expected: false},
        // Miss Upper
        {Input: &four.Assignment{Start:4, End: 10}, Expected: false},
        // Miss Both
        {Input: &four.Assignment{Start:1, End: 10}, Expected: false},
        // Hit Lower
        {Input: &four.Assignment{Start:3, End: 10}, Expected: false},
        // Hit Upper
        {Input: &four.Assignment{Start:2, End: 8}, Expected: false},
        // Single Input Lower
        {Input: &four.Assignment{Start:3, End: 3}, Expected: true},
        // Single Input Uper
        {Input: &four.Assignment{Start:8, End: 8}, Expected: true},
    }

    for _, test := range tests {
        result := static.Contains(test.Input)
        if result != test.Expected {
            t.Logf("Expected %v.contains(%v) to return %v: got %v\n", static.ToString(), test.Input.ToString(), test.Expected, result)
            t.Fail()
        }
    }
}

func TestOrderContains(t *testing.T) {
    type testCase struct {
        Input *four.Order
        Expected bool
    }

    tests := []testCase{
        // First contains second
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 4, End: 6},
            },
            Expected: true,
        },
        // Second Contains first
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 4, End: 6},
                Second: &four.Assignment{Start: 1, End: 8},
            },
            Expected: true,
        },
        // Miss
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 4, End: 10},
            },
            Expected: false,
        },
    }

    for _, test := range tests {
        result := test.Input.Contains()
        if result != test.Expected {
            t.Logf("%v, expected overlaps to return %v, got %v\n", test.Input.ToString(), test.Expected, result)
            t.Fail()
        }
    }
}

func TestAssignmentOverlaps(t *testing.T) {
    type testCase struct {
        Input *four.Assignment
        Expected bool
    }
    static := &four.Assignment{Start: 3, End: 8}
    tests := []testCase{
        // Overlap Upper
        {Input: &four.Assignment{Start:7, End: 10}, Expected: true},
        // Overlap Lower
        {Input: &four.Assignment{Start:1, End: 5}, Expected: true},
        // Covers
        {Input: &four.Assignment{Start:1, End: 10}, Expected: true},
        // Miss High
        {Input: &four.Assignment{Start:9, End: 12}, Expected: false},
        // Miss Low
        {Input: &four.Assignment{Start:1, End: 2}, Expected: false},
    }

    for _, test := range tests {
        result := static.Overlaps(test.Input)
        if result != test.Expected {
            t.Logf("Expected %v.Overlaps(%v) to return %v: got %v\n", static.ToString(), test.Input.ToString(), test.Expected, result)
            t.Fail()
        }
    }
}

func TestOrderOverlaps(t *testing.T) {

    type testCase struct {
        Input *four.Order
        Expected bool
    }

    tests := []testCase{
        // First overlaps second high
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 4, End: 10},
                Second: &four.Assignment{Start: 3, End: 8},
            },
            Expected: true,
        },
        // First overlaps second low
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 1, End: 5},
                Second: &four.Assignment{Start: 3, End: 8},
            },
            Expected: true,
        },
        // first contains second
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 1, End: 9},
                Second: &four.Assignment{Start: 3, End: 8},
            },
            Expected: true,
        },
        // Second overlaps first high
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 4, End: 10},
            },
            Expected: true,
        },
        // Second overlaps first low
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 1, End: 5},
            },
            Expected: true,
        },
        // Second contains first
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 1, End: 9},
            },
            Expected: true,
        },
        // Miss
        {
            Input: &four.Order{
                First: &four.Assignment{Start: 3, End: 8},
                Second: &four.Assignment{Start: 9, End: 12},
            },
            Expected: false,
        },
    }
    for _, test := range tests {
        result := test.Input.Overlaps()
        if result != test.Expected {
            t.Logf("%v, expected overlaps to return %v, got %v\n", test.Input.ToString(), test.Expected, result)
            t.Fail()
        }
    }
}
