package day

import (
	"fmt"
	"log"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)


type Day struct {
    input lib.AocPuzzle
    day int
    year int
    partOne func(lib.AocPuzzle) (string, error)
    partTwo func(lib.AocPuzzle) (string, error)
}

func (d *Day) Solve() {
    answerOne, err := d.partOne(d.input)
    if err != nil {
        log.Printf("Failed to proccess Day %v Part 1 with error: %v\n", d.day, err.Error())
    } else {
        fmt.Printf("Day %v Part 1: %v\n", d.day, answerOne)
    }

    answerTwo, err := d.partTwo(d.input)
    if err != nil {
        log.Printf("Failed to proccess Day %v Part 2 with error: %v\n", d.day, err.Error())
    } else {
        fmt.Printf("Day %v Part 2: %v\n", d.day, answerTwo)
    }
}

func Factory(day int , year int, partOne func(lib.AocPuzzle) (string, error), partTwo func(lib.AocPuzzle) (string, error)) (*Day, bool) {
    input, ok := lib.WebInputFactory(day, year)
    if !ok {
        log.Printf("Failed to fetch day %v input\n", day)
        return nil, false
    }
    d := &Day{
        input: input,
        day: day,
        year: year,
        partOne: partOne,
        partTwo: partTwo,
    }
    return d, true
}
