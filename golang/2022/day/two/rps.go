package two

import (
	"fmt"
	"strings"
)

// Rock Paper Scissors Sign Function
type Rps int

const (
    Rock Rps = iota
    Paper
    Scissors
    Unknown
)

func (r Rps) ToString() string {
    switch r {
        case Rock: return "Rock"
        case Scissors: return "Scissors"
        case Paper: return "Paper"
    }
    return "Unknown"
}

// Outcome determination
type Outcome int
const (
    Win Outcome = iota
    Lose
    Tie
    Undetermined
)

func (o Outcome) ToString() string {
    switch o {
        case Win: return "Win"
        case Lose: return "Lose"
        case Tie: return "Tie"
    }
    return "Unknown"
}

// Rock Paper Scissors Game
type RpsGame struct {
    Opponent Rps
    Player Rps
}

func (r *RpsGame) ToString() string {
    return fmt.Sprintf("%v v. %v", r.Opponent.ToString(), r.Player.ToString())
}
func (r *RpsGame) Won() bool {
    if r.Opponent == Rock && r.Player == Paper {
        return true
    }
    if r.Opponent == Paper && r.Player == Scissors {
        return true
    }
    if r.Opponent == Scissors && r.Player == Rock {
        return true
    }
    return false
}

func (r *RpsGame) Tie() bool {
    return r.Opponent == r.Player
}

func (r *RpsGame) Score() int {
    score := 0
    // Score sign thrown
    switch r.Player {
        case Rock: score += 1
        case Paper: score += 2
        case Scissors: score += 3
    }

    // Score if won
    if r.Won() {
        score += 6
    }
    if r.Tie() {
        score += 3
    }

    return score
}

func DeterminedRequiredSign(outcome Outcome, opponent Rps) Rps {
    if outcome == Tie {
        return opponent
    } else if outcome == Win {
        switch opponent {
            case Rock: return Paper
            case Paper: return Scissors
            case Scissors: return Rock
            default: return Unknown
        }
    } else if outcome == Lose {
        switch opponent {
            case Rock: return Scissors
            case Paper: return Rock
            case Scissors: return Paper
            default: return Unknown
        }
    }
    return Unknown
}
func NormalizeOutcomes(input string) Outcome {
    switch input {
        case "X": return Lose
        case "Y": return Tie
        case "Z": return Win
    }
    return Undetermined
}

func NormalizeSigns(input string) Rps {
    switch input {
        case "A": return Rock
        case "B": return Paper
        case "C": return Scissors
        case "X": return Rock
        case "Y": return Paper
        case "Z": return Scissors
    }
    // Shouldnt reach this lets log it incase
    return Unknown
}

func RpsGameFactory(input string, byOutcome bool) *RpsGame {
    splits := strings.Split(input, " ")
    if len(splits) != 2 {
        return nil
    }
    opp := NormalizeSigns(splits[0])

    // Assume XYZ translate to literal signs to throw
    if !byOutcome{
        player := NormalizeSigns(splits[1])

        return &RpsGame{
            Opponent: opp,
            Player: player,
        }
    }

    // Assume XYZ translate to match outcomes
    playerOutcome := NormalizeOutcomes(splits[1])
    player := DeterminedRequiredSign(playerOutcome, opp)
    return &RpsGame{
        Opponent: opp,
        Player: player,
    }
}
