package two

import (
	"log"
    "fmt"
	"sync"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
)

func buildGames(input lib.AocPuzzle, byOutcome bool) ([]*RpsGame, error) {
    i, err := input.GetInput()
    if err != nil {
        return nil, err
    }
    games := []*RpsGame{}
    for i, v := range i{
        if v == "" {
            continue
        }
        newGame := RpsGameFactory(v, byOutcome)
        if newGame == nil {
            err := &day.AocError{
                ErrorString: fmt.Sprintf("Failed to parse RPS Game from: `%v`(line %v)", v, i),
            }
            return nil, err
        }
        games = append(games, newGame)
    }
    return games, nil
}

func calculateScore(games []*RpsGame) int {
    total := 0
    for _, game := range games {
        total += game.Score()
    }
    return total

}

func PartOne(input lib.AocPuzzle) (string, error) {
    games, err := buildGames(input, false)
    if err != nil {
        return "-1", err
    }
    return fmt.Sprintf("%v", calculateScore(games)), nil
}

func PartTwo(input lib.AocPuzzle) (string, error) {
    games, err := buildGames(input, true)
    if err != nil {
        return "-1", err
    }
    return fmt.Sprintf("%v", calculateScore(games)), nil
}

func Solve(wg *sync.WaitGroup) {
    defer wg.Done()
    d, ok := day.Factory(2, 2022, PartOne, PartTwo)
    if !ok {
        log.Printf("Failed to initialize day 2\n")
        return
    }
    d.Solve()
}
