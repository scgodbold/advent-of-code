package two_test

import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/lib"
	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/two"
)

func getSampleTwo() lib.AocPuzzle {
    return &lib.SimpleInput{
        Input: []string{
            "A Y",
            "B X",
            "C Z",
        },
    }
}

func TestSampleInputPartOne(t *testing.T) {
    input := getSampleTwo()
    expected := "15"

    result, err := two.PartOne(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}

func TestSampleInputPartTwo(t *testing.T) {
    input := getSampleTwo()
    expected := "12"

    result, err := two.PartTwo(input)
    if err != nil {
        t.Logf("Errors should be nil but got %v\n", err.Error())
        t.Fail()
    }

    if result != expected {
        t.Logf("Should return %v but got %v\n",expected, result)
        t.Fail()
    }
}
