package two_test


import (
    "testing"

	"gitlab.com/scgodbold/advent-of-code/golang/2022/day/two"
)


func TestNormalizeInput(t *testing.T) {
    type test struct {
        Input string
        Expected two.Rps
    }

    tests := []test{
        {Input: "X", Expected: two.Rock},
        {Input: "Y", Expected: two.Paper},
        {Input: "Z", Expected: two.Scissors},
        {Input: "A", Expected: two.Rock},
        {Input: "B", Expected: two.Paper},
        {Input: "C", Expected: two.Scissors},
    }

    for _, testCase := range tests {
        result := two.NormalizeSigns(testCase.Input)
        if result != testCase.Expected {
            t.Logf("Input: %v, Expected %v but got %v\n", testCase.Input, testCase.Expected.ToString(), result.ToString())
            t.Fail()
        }
    }
}

func TestGameWon(t *testing.T) {
    type test struct {
        Input *two.RpsGame
        Expected bool
    }

    tests := []test{
        // Paper
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Paper}, Expected: true},
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Paper}, Expected: false},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Paper}, Expected: false},
        // Rock
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Rock}, Expected: false},
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Rock}, Expected: false},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Rock}, Expected: true},
        // Scissors
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Scissors}, Expected: false},
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Scissors}, Expected: true},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Scissors}, Expected: false},
    }

    for _, testCase := range tests {
        result := testCase.Input.Won()
        if result != testCase.Expected {
            t.Logf("Input: %v, Expected %v but got %v\n", testCase.Input.ToString(), testCase.Expected, result)
            t.Fail()
        }
    }
}

func TestGameScoring(t *testing.T) {
    type test struct {
        Input *two.RpsGame
        Expected int
    }
    tests := []test{
        // Won Scenarios
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Scissors}, Expected: 9},
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Paper}, Expected: 8},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Rock}, Expected: 7},
        // Lost Scenarios
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Rock}, Expected: 1},
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Scissors}, Expected: 3},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Paper}, Expected: 2},
        // Tie Scenarios
        {Input: &two.RpsGame{Opponent: two.Paper, Player:two.Paper}, Expected: 5},
        {Input: &two.RpsGame{Opponent: two.Rock, Player:two.Rock}, Expected: 4},
        {Input: &two.RpsGame{Opponent: two.Scissors, Player:two.Scissors}, Expected: 6},
    }
    for _, testCase := range tests {
        result := testCase.Input.Score()
        if result != testCase.Expected {
            t.Logf("Input: %v, Expected %v but got %v\n", testCase.Input.ToString(), testCase.Expected, result)
            t.Fail()
        }
    }
}

func TestNormalizeOutcome(t *testing.T) {
    type test struct {
        Input string
        Expected two.Outcome
    }

    tests := []test{
        {Input: "X", Expected: two.Lose},
        {Input: "Y", Expected: two.Tie},
        {Input: "Z", Expected: two.Win},
    }
    for _, testCase := range tests {
        result := two.NormalizeOutcomes(testCase.Input)
        if result != testCase.Expected {
            t.Logf("Input: %v, Expected %v but got %v\n", testCase.Input, testCase.Expected.ToString(), result.ToString())
            t.Fail()
        }
    }
}

func TestDetrmineRequiredOutcome(t *testing.T) {
    type test struct {
        Outcome two.Outcome
        Opponent two.Rps
        Expected two.Rps
    }

    tests := []test{
        // Win Conditions
        {Outcome: two.Win, Opponent: two.Rock, Expected: two.Paper},
        {Outcome: two.Win, Opponent: two.Paper, Expected: two.Scissors},
        {Outcome: two.Win, Opponent: two.Scissors, Expected: two.Rock},
        // Lose Conditions
        {Outcome: two.Lose, Opponent: two.Rock, Expected: two.Scissors},
        {Outcome: two.Lose, Opponent: two.Paper, Expected: two.Rock},
        {Outcome: two.Lose, Opponent: two.Scissors, Expected: two.Paper},
        // Tie Conditions
        {Outcome: two.Tie, Opponent: two.Rock, Expected: two.Rock},
        {Outcome: two.Tie, Opponent: two.Paper, Expected: two.Paper},
        {Outcome: two.Tie, Opponent: two.Scissors, Expected: two.Scissors},
    }

    for _, testCase := range tests {
        result := two.DeterminedRequiredSign(testCase.Outcome, testCase.Opponent)
        if result != testCase.Expected {
            t.Logf("Input: (%v, %v), Expected %v but got %v\n", testCase.Outcome.ToString(), testCase.Opponent.ToString(), testCase.Expected.ToString(), result.ToString())
            t.Fail()
        }
    }
}
