package day

type AocError struct {
    ErrorString string
}

func (e *AocError) Error() string {
    return e.ErrorString
}
